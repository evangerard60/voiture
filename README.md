# Projet 151 Voiture


## Gestion de données de voiture 

### Descritpion 
Sur ce projet nous pourrons voir toutes les informations de toutes les voitures, que ce soit des voitures de **course** comme des voitures de **tout les jour**. En plus de cela, les utilisateurs pourrons eux même **rajouter** des voiture et seront mise sur le site juste après. UNe verification sera faite et si les informations ne sont pas bonnes elles seront modifier ou suppprimé par les perosnnes ayant les droits.

### Prototype 
Sur le prototype nous pourrons voir un seule voiture avec toutes les informations possible sur la voiture, tel que la **puissance**, le **couple** ou encore les **années de production**. Nous pourrons aussi **ajouter** quelques voiture et en **supprimer**. Il sera aussi possible de voir dans quel course la voiture a courrue et par qui elle a été conduite.

### Communication 
|                                            |Requete HTTP|Page Cible  |Données transmises                 |
|--------------------------------------------|------------|-----------------|------------------------------|
|Affichage de la page d'accueil              |Get         |index.php        |                              |
|Affichage de la page de la fiche de voitures|Get         |fichevoiture.php |id-voiture                    |
|Affichage de la page d'ajout de voitures    |Get         |addvoiture.php   |                              |
|Ajout de voitures sur le site               |Post        |fichevoiture.php |Toutes les infos de la voiture|
|Supression de voitures sur le site          |Delete      |fichevoiture.php |id-voiture                    |
### Base de données

Dans mon projet il y a 3 tables, certe pas enorme mais suffisant pour des voiture. 

|Voiture| |Pilote       | |Circuit     |
|-------| |-------------| |------------|
|IdCars | |IdPilote     | |IdCircuit   |
|Brand  | |Nom          | |Nom         |
|Model  | |Prenom       | |Distance    |
|Chassis| |DateNaissance| |DateCreation|
|HP     |                 |TotalCourses|
|Torque |
|Engine |
|Accel  |
|Brake  |
|Rupteur|
